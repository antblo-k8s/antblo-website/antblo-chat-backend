FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 5001

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS debug
WORKDIR /app
RUN apt update -y && apt install -y curl iptables sudo sshfs mono-devel
RUN sudo curl -fLk https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence
RUN sudo chmod a+x /usr/local/bin/telepresence

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app
COPY ["antblo-chat-backend.csproj", "./"]
RUN dotnet restore "antblo-chat-backend.csproj"
COPY . .
WORKDIR "/app/."
RUN dotnet build "antblo-chat-backend.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "antblo-chat-backend.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ASPNETCORE_URLS=http://+:5001
ENTRYPOINT ["dotnet", "antblo-chat-backend.dll"]
