using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

public class JwtValidator
{
  private TokenValidationParameters validationParameters;
  private JwtSecurityTokenHandler handler;

  public JwtValidator()
  {
    var realmUrl = "http://keycloak.keycloak/auth/realms/antblo";
    var authEndpointUrl = realmUrl + "/.well-known/openid-configuration/";
    var openIdConfigRetriever = new OpenIdConnectConfigurationRetriever();
    var docRetriever = new HttpDocumentRetriever() { RequireHttps = false };
    var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(authEndpointUrl, openIdConfigRetriever, docRetriever);
    OpenIdConnectConfiguration openIdConfig = configurationManager.GetConfigurationAsync().Result;
    this.validationParameters = new TokenValidationParameters
    {
      ValidateIssuer = false,
      ValidAudiences = new[] { "account" },
      IssuerSigningKeys = openIdConfig.SigningKeys,
    };
    this.handler = new JwtSecurityTokenHandler();
  }

  public ClaimsPrincipal? validateToken(string token)
  {
    try
    {
      SecurityToken validatedToken;
      var user = handler.ValidateToken(token, validationParameters, out validatedToken);
      return user;
    }
    catch (Exception e)
    {
      Console.Error.WriteLine("Could not validate token.");
      Console.Error.WriteLine(e);
      return null;
    }
  }
}