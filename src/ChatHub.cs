using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ChatDB;
using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace SignalRChat.Hubs
{
  class ClientWrapper
  {
    public readonly IHubCallerClients clients;

    public ClientWrapper(IHubCallerClients clients)
    {
      this.clients = clients;
    }
  }
  public class ChatHub : Hub
  {
    private DiscordSocketClientWrapper discordBot;
    private readonly ChatDBContext chatDB;
    private JwtValidator jwtValidator;
    Regex bad_chars_regex = new Regex("[^a-zA-Z0-9]+");

    public ChatHub(DiscordSocketClientWrapper discordBot, ChatDBContext chatDB, JwtValidator jwtValidator)
    {
      this.discordBot = discordBot;
      this.chatDB = chatDB;
      this.jwtValidator = jwtValidator;
    }

    public async Task SendMessage(string message)
    {
      var conn = await this.chatDB.Connections.FirstOrDefaultAsync(x => x.ConnString == Context.ConnectionId);
      var user = conn.User;
      if (user == null) return;
      await discordBot.SendMessage(user.Username, message);
    }

    public async Task Authorization(string token)
    {
      // Make sure the user is who they say they are.
      var accessToken = this.jwtValidator.validateToken(token);
      if (accessToken != null)
      {
        // Find the user's username from the claims.
        foreach (var claim in accessToken.Claims)
        {
          if (claim.Type.Equals("preferred_username"))
          {
            var username = claim.Value;
            username = this.bad_chars_regex.Replace(username, "-"); // Discord doesn't tollerate most symbols.
            var user = await chatDB.Users.Include(x => x.Connections).FirstOrDefaultAsync(x => x.Username == username);
            if (user == null)
            {
              user = new User() { Username = username, Connections = new List<Connection>() };
              await this.chatDB.Users.AddAsync(user);
              await this.chatDB.SaveChangesAsync();
            }
            var conn = new Connection() { ConnString = Context.ConnectionId, User = user };
            user.Connections.Add(conn);
            await this.chatDB.SaveChangesAsync();
            var aaa = await chatDB.Users.FirstOrDefaultAsync(x => x.Username == "test");
            var lastHundredMessages = await this.discordBot.CreateChannel(username);
            foreach (IMessage message in lastHundredMessages)
            {
              if (message.Author.Username == "antblo-chat")
                await Clients.Caller.SendAsync("ReceiveMessage", username, message.Content);
              else
                await Clients.Caller.SendAsync("ReceiveMessage", "Anton", message.Content);
            }
          }
        }
      }
    }

    override public async Task OnDisconnectedAsync(Exception? exception)
    {
      var conn = await this.chatDB.Connections.FirstOrDefaultAsync(x => x.ConnString == Context.ConnectionId);
      if (conn != null)
      {
        this.chatDB.Connections.Remove(conn);
        await this.chatDB.SaveChangesAsync();
      }
      await base.OnDisconnectedAsync(exception);
    }
  }
}
