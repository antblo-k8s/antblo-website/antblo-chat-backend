using System;
using ChatDB;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace antblo_chat_backend_build
{
  public class Program
  {
    public static void LoadEnvs()
    {
      DotNetEnv.Env.Load("/intercept.env");
    }
    public static void Main(string[] args)
    {
      LoadEnvs();
      var host = CreateHostBuilder(args).Build();

      CreateDbIfNotExists(host);

      host.Run();
    }

    private static void CreateDbIfNotExists(IHost host)
    {
      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;
        try
        {
          var context = services.GetRequiredService<ChatDBContext>();
          context.Database.EnsureCreated();
          // DbInitializer.Initialize(context);
        }
        catch (Exception ex)
        {
          Console.Error.WriteLine("An error occurred creating the DB.");
          Console.Error.WriteLine(ex);
        }
      }
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder.UseStartup<Startup>();
            });
  }
}
