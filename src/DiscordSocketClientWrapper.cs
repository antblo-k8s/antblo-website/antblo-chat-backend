using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatDB;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using SignalRChat.Hubs;

public class DiscordSocketClientWrapper
{
  private DiscordSocketClient client;
  private bool guildAvailable;

  private SocketGuild guild { get { return this.client.GetGuild(828735035515207740); } } // A "Guild" is a collection of channels where users can chat in.
  public bool IsConnected() => this.client.ConnectionState.Equals(ConnectionState.Connected);

  public DiscordSocketClientWrapper(ChatDBContext chatDB, IHubContext<ChatHub> hubContext)
  {
    this.client = new DiscordSocketClient(new DiscordSocketConfig { MessageCacheSize = 100 });
    var token = Environment.GetEnvironmentVariable("discord_secret");
    if (token == null)
    {
      Console.Error.WriteLine("Discord secret was null.");
      return;
    }
    this.guildAvailable = false;
    this.client.GuildAvailable += (SocketGuild socketGuild) =>
    {
      this.guildAvailable = true;
      return Task.CompletedTask;
    };
    this.client.LoginAsync(TokenType.Bot, token).GetAwaiter().GetResult();
    this.client.StartAsync().GetAwaiter().GetResult();

    this.client.MessageReceived += async (SocketMessage msg) =>
      {
        using (var chatDB = new ChatDBContext())
        {
          var user = await chatDB.Users.Include(x => x.Connections).FirstOrDefaultAsync(x => x.Username == msg.Channel.Name);
          if (user == null) return;
          foreach (var conn in user.Connections)
          {
            var chatClient = hubContext.Clients.Client(conn.ConnString);

            if (msg.Author.Username == "antblo-chat")
              await chatClient.SendAsync("ReceiveMessage", user.Username, msg.Content);
            else
              await chatClient.SendAsync("ReceiveMessage", "Anton", msg.Content);
          }
        }
      };
  }

  internal async Task SendMessage(string userChannel, string msg)
  {
    foreach (var channel in this.guild.TextChannels)
    {
      if (channel.Name == userChannel)
      {
        await channel.SendMessageAsync(msg);
      }
    }
  }

  internal async Task<IEnumerable<IMessage>> CreateChannel(string username)
  {
    while (!this.guildAvailable) await Task.Delay(100);
    foreach (var userChannel in this.guild.TextChannels)
    {
      if (userChannel.Name == username)
      {
        var lastHundredMessages = userChannel.GetMessagesAsync();

        return (await lastHundredMessages.FlattenAsync()).Reverse();
      }
    }
    await this.guild.CreateTextChannelAsync(username);
    return new List<RestMessage>();
  }
}
