using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ChatDB
{
  public class ChatDBContext : DbContext
  {
    public DbSet<User> Users { get; set; }
    public DbSet<Connection> Connections { get; set; }

    // The following configures EF to create a Sqlite database file as `C:\blogging.db`.
    // For Mac or Linux, change this to `/tmp/blogging.db` or any other absolute path.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite(@"Data Source=./chat.db");
  }

  public class User
  {
    public int Id { get; set; }
    public String Username { get; set; }
    public ICollection<Connection> Connections { get; set; }
  }

  public class Connection
  {
    public int Id { get; set; }
    public User? User { get; set; }
    public String ConnString { get; set; }
  }
}